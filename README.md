# Projet GitLab du retour d'expérience Jupyter avec GitLab

![Licence CC BY SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

## Liens utiles

* [Les slides](https://gtdonnees-gitlab2023.gricad-pages.univ-grenoble-alpes.fr/rexp-notebooks/index.html) - [le PDF](https://gtdonnees-gitlab2023.gricad-pages.univ-grenoble-alpes.fr/rexp-notebooks/index.pdf)
* [L'interface JupyterLite](https://gtdonnees-gitlab2023.gricad-pages.univ-grenoble-alpes.fr/rexp-notebooks/jupyterlite)
* [Le projet GitLab](https://gricad-gitlab.univ-grenoble-alpes.fr/gtdonnees-gitlab2023/rexp-notebooks/pages)

## Qu'y-a-t-il ici ? 

Dans ce dépôt, il y a :
* D'une part, les sources (en markdown + images) d'une présentation HTML (et PDF) généré par la CI/CD grâce à marp
* D'autre part, les sources pour générer un site jupyterLite, génération faaite dans la CI/CD également

Les deux sites sont accessibles via l'URL de GitLab pages pour ce projet, le premier à la racine, et jupyterlite sous `url_pages + /jupyterlite`. 

